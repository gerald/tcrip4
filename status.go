package tcrip4

import "fmt"

type Status struct {
	Channel1 bool `xml:"led1"`
	Channel2 bool `xml:"led2"`
	Channel3 bool `xml:"led3"`
	Channel4 bool `xml:"led4"`
}

func (s *Status) IsOn(channel int) bool {
	if channel == 1 {
		return s.Channel1
	}
	if channel == 2 {
		return s.Channel2
	}
	if channel == 3 {
		return s.Channel3
	}
	if channel == 4 {
		return s.Channel4
	}
	return false
}

func (s *Status) String() string {
	var channel1Status, channel2Status, channel3Status, channel4Status string

	if s.Channel1 == true {
		channel1Status = "on"
	} else {
		channel1Status = "off"
	}
	if s.Channel2 == true {
		channel2Status = "on"
	} else {
		channel2Status = "off"
	}
	if s.Channel3 == true {
		channel3Status = "on"
	} else {
		channel3Status = "off"
	}
	if s.Channel4 == true {
		channel4Status = "on"
	} else {
		channel4Status = "off"
	}

	return fmt.Sprintf("Channel 1: %s\nChannel 2: %s\nChannel 3: %s\nChannel 4: %s",
		channel1Status, channel2Status, channel3Status, channel4Status)
}
