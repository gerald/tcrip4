package tcrip4

import (
	"encoding/base64"
	"encoding/xml"
	"fmt"
	"io"
	"net/http"
)

type TCRIP4 struct {
	tcrip4Host          string
	authorizationHeader string
}

func NewTCRIP4(tcrip4Host, username, password string) (*TCRIP4, error) {
	if tcrip4Host == "" {
		return nil, fmt.Errorf("Invalid host: host cannot be empty")
	}

	authHeader := ""

	if username != "" || password != "" {
		authHeader = "Basic " + base64.StdEncoding.EncodeToString([]byte(username+":"+password))
	}

	return &TCRIP4{
		tcrip4Host:          tcrip4Host,
		authorizationHeader: authHeader,
	}, nil
}

func (t *TCRIP4) request(path string) ([]byte, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("http://%s/%s", t.tcrip4Host, path), nil)
	if err != nil {
		return nil, err
	}
	if t.authorizationHeader != "Basic " {
		req.Header.Set("Authorization", t.authorizationHeader)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("Status error: %v", resp.StatusCode)
	}

	return io.ReadAll(resp.Body)
}

func (t *TCRIP4) GetStatus() (Status, error) {
	data, err := t.request("status.xml")
	if err != nil {
		return Status{}, err
	}

	status := Status{}
	err = xml.Unmarshal(data, &status)
	if err != nil {
		return Status{}, fmt.Errorf("Unmarshal XML body: %v", err)
	}

	return status, nil
}

func (t *TCRIP4) switchChannel(channel int) error {
	_, err := t.request(fmt.Sprintf("leds.cgi?led=%d", channel))
	return err
}

func (t *TCRIP4) SwitchChannel(channel int, channelShouldBeOn bool) error {
	if channel < 1 || channel > 4 {
		return fmt.Errorf("Invalid channel number. Allowed 1 - 4")
	}

	status, err := t.GetStatus()
	if err != nil {
		return err
	}

	if channelShouldBeOn != status.IsOn(channel) {
		err = t.switchChannel(channel)
		if err != nil {
			return err
		}
	}

	return nil
}
