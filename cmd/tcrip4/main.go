package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"os"
	"strconv"

	"codeberg.org/gerald/tcrip4"
	"github.com/genuinetools/pkg/cli"
)

var (
	tcrHostFlag  string
	userFlag     string
	passwordFlag string
	channelFlag  string
	tcrInstance  *tcrip4.TCRIP4
)

const statusHelp = "Display the status of the device."

type statusCommand struct{}

func (cmd *statusCommand) Name() string              { return "status" }
func (cmd *statusCommand) Args() string              { return "" }
func (cmd *statusCommand) ShortHelp() string         { return statusHelp }
func (cmd *statusCommand) LongHelp() string          { return statusHelp }
func (cmd *statusCommand) Hidden() bool              { return false }
func (cmd *statusCommand) Register(fs *flag.FlagSet) {}
func (cmd *statusCommand) Run(ctx context.Context, args []string) error {
	status, err := tcrInstance.GetStatus()
	if err != nil {
		return err
	}
	fmt.Fprintln(os.Stdout, status.String())
	return nil
}

const switchHelp = "Switch a channel of the device."

type switchCommand struct{}

func (cmd *switchCommand) Name() string      { return "switch" }
func (cmd *switchCommand) Args() string      { return "" }
func (cmd *switchCommand) ShortHelp() string { return statusHelp }
func (cmd *switchCommand) LongHelp() string  { return statusHelp }
func (cmd *switchCommand) Hidden() bool      { return false }
func (cmd *switchCommand) Register(fs *flag.FlagSet) {
	fs.StringVar(&channelFlag, "channel", "", "Channel to use")
	fs.StringVar(&channelFlag, "c", "", "Channel to use")
}
func (cmd *switchCommand) Run(ctx context.Context, args []string) error {
	channelToSwitch, err := strconv.Atoi(channelFlag)
	if err != nil {
		return err
	}

	if len(args) != 1 {
		return fmt.Errorf("Exactly one argument required on/off")
	}

	var channelShouldBeOn bool
	if args[0] == "on" {
		channelShouldBeOn = true
	} else if args[0] == "off" {
		channelShouldBeOn = false
	} else {
		return fmt.Errorf("Invalid request. Allowed on, off")
	}

	return tcrInstance.SwitchChannel(channelToSwitch, channelShouldBeOn)
}

func main() {
	// Create a new cli program.
	p := cli.NewProgram()
	p.Name = "tcrip4"
	p.Description = "Control a TCR IP 4"

	// Setup the global flags.
	p.FlagSet = flag.NewFlagSet("global", flag.ExitOnError)
	p.FlagSet.StringVar(&tcrHostFlag, "tcr-host", os.Getenv("TCR_HOST"), "Host of the TCR IP 4 (or env var TCR_HOST)")
	p.FlagSet.StringVar(&tcrHostFlag, "a", os.Getenv("TCR_HOST"), "Host of the TCR IP 4 (or env var TCR_HOST)")

	p.FlagSet.StringVar(&userFlag, "user", os.Getenv("TCR_USER"), "Username of the TCR IP 4 (or env var TCR_USER)")
	p.FlagSet.StringVar(&userFlag, "u", os.Getenv("TCR_USER"), "Username of the TCR IP 4 (or env var TCR_USER)")

	p.FlagSet.StringVar(&passwordFlag, "password", os.Getenv("TCR_PASSWORD"), "Password of the TCR IP 4 (or env var TCR_PASSWORD)")
	p.FlagSet.StringVar(&passwordFlag, "p", os.Getenv("TCR_PASSWORD"), "Password of the TCR IP 4 (or env var TCR_PASSWORD)")

	// Set the before function.
	p.Before = func(ctx context.Context) error {
		if tcrHostFlag == "" {
			return errors.New("TCR IP 4 host cannot be empty")
		}

		var err error
		tcrInstance, err = tcrip4.NewTCRIP4(tcrHostFlag, userFlag, passwordFlag)

		return err
	}

	p.Commands = []cli.Command{
		&switchCommand{},
		&statusCommand{},
	}

	// Run our program.
	p.Run()
}
